package APIUtils.enums;

public enum RegionFilters {
    SUCEAVA("Romania", "Suceava"),
    BUCURESTI("Romania", "Bucuresti"),
    IASI("Romania", "Iasi"),
    CONSTANTA("Romania", "Constanta"),
    BERLIN("Germany", "Berlin");
    private final String country;
    private final String region;

    RegionFilters(String country, String region) {
        this.country = country;
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public String getRegion() {
        return region;
    }
}
