package APIUtils;

import APIUtils.actions.APITestDTO;
import APIUtils.responses.Location;
import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static io.restassured.RestAssured.given;


public class APIUtilis {
    private final static String WEATHER_API_URL = "http://api.weatherapi.com/v1/current.json?";
    private final static String API_KEY_ACCESS = "e8700cf729044f15b3d130035211508";
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final Date date = new Date();

    public static Location executeGetActionLocation(APITestDTO parameters) {
        String url = WEATHER_API_URL + "KEY=" + API_KEY_ACCESS + "&" + ObjectUtils.convertObjectToQueryString(parameters);
        System.out.println("Making call to: " + url);
        Response response = null;
        response = given()
                .contentType(ContentType.JSON)
                .get(url);
        System.out.println("Response: " + response.getBody().asString());
        System.out.println("Status code: " + response.getStatusCode() + "\n");

        Gson gson = new Gson();

        return gson.fromJson(response.asString(), Location.class);
    }

    public static String timeZoneRomania() {
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }

    public static String timeZoneGermany() {
        df.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        return df.format(date);
    }

}
