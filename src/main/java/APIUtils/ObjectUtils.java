package APIUtils;

import java.lang.reflect.Field;

public class ObjectUtils {

    public static String convertObjectToQueryString(Object obj) {
        StringBuilder queryString = new StringBuilder();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value = null;
            try {
                value = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (value == null) {
                continue;
            }
            queryString.append(field.getName() + "=" + value.toString() + "&");
        }
        String query = queryString.substring(0, queryString.length() - 1);
        System.out.println("QueryString parameters: " + query);
        return query;
    }

}

