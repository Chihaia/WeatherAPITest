package APIUtils.responses;

import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.annotations.Expose;
import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.annotations.SerializedName;


public class Location {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("region")
    @Expose
    public String region;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("lat")
    @Expose
    public Double lat;
    @SerializedName("lon")
    @Expose
    public Double lon;
    @SerializedName("tz_id")
    @Expose
    public String tzId;
    @SerializedName("localtime_epoch")
    @Expose
    public Integer localtimeEpoch;
    @SerializedName("localtime")
    @Expose
    public String localtime;
    @SerializedName("location")
    @Expose
    public Location location;

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getTzId() {
        return tzId;
    }

    public Integer getLocaltimeEpoch() {
        return localtimeEpoch;
    }

    public String getLocaltime() {
        return localtime;
    }
}
