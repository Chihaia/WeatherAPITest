package APIUtils.actions;

public class APITestDTO {
    private String q;

    public void setLocation(String q) {
        this.q = q;
    }

    public static final class APITestDTOBuilder {
        private String q;

        public APITestDTOBuilder() {
        }

        public static APITestDTOBuilder aAPITestDTO() {
            return new APITestDTOBuilder();
        }

        public APITestDTOBuilder withLocation(String q) {
            this.q = q;
            return this;
        }

        public APITestDTO build() {
            APITestDTO apitestDTO = new APITestDTO();
            apitestDTO.setLocation(q);
            return apitestDTO;
        }
    }
}
