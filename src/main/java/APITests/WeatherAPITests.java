package APITests;

import APIUtils.APIUtilis;
import APIUtils.actions.APITestDTO;
import APIUtils.enums.RegionFilters;
import APIUtils.responses.Location;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;


public class WeatherAPITests {
    @Rule
    public TestName name = new TestName();
    public static SoftAssertions softly = new SoftAssertions();

    @Before
    public void initBeforeTest() {
        System.out.println(name.getMethodName());
    }

    @After
    public void clean() {
        System.out.println(name.getMethodName());
        softly.assertAll();
    }

    @Test
    public void testLocationAPIWeatherBucharest() {
        APITestDTO object = new APITestDTO.APITestDTOBuilder()
                .withLocation(RegionFilters.BUCURESTI.getRegion())
                .build();
        Location response = APIUtilis.executeGetActionLocation(object);
        softly.assertThat(response.getLocation().getCountry())
                .describedAs("THE COUNTRY IS NOT CORRECT")
                .isEqualTo(RegionFilters.BUCURESTI.getCountry());
        softly.assertThat(response.getLocation().getLocaltime())
                .describedAs("THE TIMEZONE IS NOT CORRECT")
                .isEqualTo(APIUtilis.timeZoneRomania());
    }

    @Test
    public void testLocationAPIWeatherSuceava() {
        APITestDTO object = new APITestDTO.APITestDTOBuilder()
                .withLocation(RegionFilters.SUCEAVA.getRegion())
                .build();
        Location response = APIUtilis.executeGetActionLocation(object);
        softly.assertThat(response.getLocation().getCountry())
                .describedAs("THE COUNTRY IS NOT CORRECT")
                .isEqualTo(RegionFilters.SUCEAVA.getCountry());
        softly.assertThat(response.getLocation().getLocaltime())
                .describedAs("THE TIMEZONE IS NOT CORRECT")
                .isEqualTo(APIUtilis.timeZoneRomania());
    }

    @Test
    public void testLocationAPIWeatherIasi() {
        APITestDTO object = new APITestDTO.APITestDTOBuilder()
                .withLocation(RegionFilters.IASI.getRegion())
                .build();
        Location response = APIUtilis.executeGetActionLocation(object);
        softly.assertThat(response.getLocation().getCountry())
                .describedAs("THE COUNTRY IS NOT CORRECT")
                .isEqualTo(RegionFilters.IASI.getCountry());
        softly.assertThat(response.getLocation().getLocaltime())
                .describedAs("THE TIMEZONE IS NOT CORRECT")
                .isEqualTo(APIUtilis.timeZoneRomania());
    }

    @Test
    public void testLocationAPIWeatherConstanta() {
        APITestDTO object = new APITestDTO.APITestDTOBuilder()
                .withLocation(RegionFilters.CONSTANTA.getRegion())
                .build();
        Location response = APIUtilis.executeGetActionLocation(object);
        softly.assertThat(response.getLocation().getCountry())
                .describedAs("THE COUNTRY IS NOT CORRECT")
                .isEqualTo(RegionFilters.CONSTANTA.getCountry());
        softly.assertThat(response.getLocation().getLocaltime())
                .describedAs("THE TIMEZONE IS NOT CORRECT")
                .isEqualTo(APIUtilis.timeZoneRomania());
    }

    @Test
    public void testLocationAPIWeatherBerlin() {
        APITestDTO object = new APITestDTO.APITestDTOBuilder()
                .withLocation(RegionFilters.BERLIN.getRegion())
                .build();
        Location response = APIUtilis.executeGetActionLocation(object);
        softly.assertThat(response.getLocation().getCountry())
                .describedAs("THE COUNTRY IS NOT CORRECT")
                .isEqualTo(RegionFilters.BERLIN.getCountry());
        softly.assertThat(response.getLocation().getLocaltime())
                .describedAs("THE TIMEZONE IS NOT CORRECT")
                .isEqualTo(APIUtilis.timeZoneGermany());
    }

}

