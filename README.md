A simple project where using Java and Selenium to test API Endpoint of: http://api.weatherstack.com/ 

Modules used: 
- selenium-java 3.141.5
- rest-assured 4.2.0
- junit 4.12

Browser: chromedriver.exe

